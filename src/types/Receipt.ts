import type { Member } from "./Member";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";

const defaultReceipt = {
  id: -1,
  createdDate: new Date(),
  total: 0,
  receivedAmount: 0,
  change: 0,
  paymentType: 'cash',
  employeeId: 1,
  memberId: null
}
type Receipt = {
  id: number;
  createdDate: Date;
  total: number;
  amount: number;
  change: number;
  paymentType: string;
  userId: number;
  memberId: number;
  receiptItems?: ReceiptItem[]
  user?: User
  member?: Member
}

export { type Receipt }