import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'


export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([{ id: 1, name: 'A', tel: '088888881' }, { id: 2, name: 'B', tel: '088888882' }])
  const getMemberByTel = (tel: string): Member | null => {

    for (const member of members.value) {
      if (member.tel === tel)
        return member
    }
    return null
  }
  return { members, getMemberByTel }
})
