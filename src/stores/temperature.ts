import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import { useLoadingStore } from './loading'
import http from '@/services/http'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
    const valid = ref(false)
    const celsius = ref(0)
    const result = ref(0)
    const LoadingStore = useLoadingStore()

    async function callconvert() {
        console.log('Store: call convert')
        // result.value = convert(celsius.value)
        LoadingStore.doLoad()
        try {
            result.value = await temperatureService.convert(celsius.value)
        } catch (e) {
            console.log(e)
        }
        LoadingStore.finish()
        console.log('Store: Finish call convert')
    }

    return { valid, result, celsius, callconvert }
})
